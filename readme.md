Steps to configure LEMP

Download the scripts

Navigate into the containing folder (the folder name should be lemp-configuration-scripts if you cloned it via git)
        cd lemp-configuration-scripts/

to add a Wordpress website create a user get the path for the website to be installed onto (this needs to be input for the script) 
run the following command from the scripts folder
        ./createWPNginxVhost.sh

to tune percona it needs to run for some time. then run
        ./mysqltuner.pl

to restart services
        ./servicesrestart.sh

Manually done to allow external access
        yum install vsftpd